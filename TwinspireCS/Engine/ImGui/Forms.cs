﻿using Raylib_cs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TwinspireCS.Engine.Graphics;
using IG = ImGuiNET;

namespace TwinspireCS.Engine.ImGui
{
    public class Forms
    {

        public static bool FieldCombo(string id, string text, ref int selectedItem, int width, string[] array)
        {
            IG.ImGui.Text(text); IG.ImGui.SameLine();
            IG.ImGui.SetNextItemWidth(width);
            return IG.ImGui.Combo("##" + id, ref selectedItem, array, array.Length);
        }

        public static bool FieldInt(string id, string text, ref int value, int width)
        {
            IG.ImGui.Text(text); IG.ImGui.SameLine();
            IG.ImGui.SetNextItemWidth(width);
            return IG.ImGui.InputInt("##" + id, ref value);
        }

        public static bool FieldInt(string id, string text, ref int value, int width, int step)
        {
            IG.ImGui.Text(text); IG.ImGui.SameLine();
            IG.ImGui.SetNextItemWidth(width);
            return IG.ImGui.InputInt("##" + id, ref value, step);
        }

        public static bool FieldInt(string id, string text, ref int value, int width, int step, int step_fast)
        {
            IG.ImGui.Text(text); IG.ImGui.SameLine();
            IG.ImGui.SetNextItemWidth(width);
            return IG.ImGui.InputInt("##" + id, ref value, step, step_fast);
        }

        public static bool FieldFloat(string id, string text, ref float value, int width)
        {
            IG.ImGui.Text(text); IG.ImGui.SameLine();
            IG.ImGui.SetNextItemWidth(width);
            return IG.ImGui.InputFloat("##" + id, ref value);
        }

        public static bool FieldFloat(string id, string text, ref float value, int width, float step)
        {
            IG.ImGui.Text(text); IG.ImGui.SameLine();
            IG.ImGui.SetNextItemWidth(width);
            return IG.ImGui.InputFloat("##" + id, ref value, step);
        }

        public static bool FieldFloat(string id, string text, ref float value, int width, float step, float step_fast)
        {
            IG.ImGui.Text(text); IG.ImGui.SameLine();
            IG.ImGui.SetNextItemWidth(width);
            return IG.ImGui.InputFloat("##" + id, ref value, step, step_fast);
        }

        public static bool FieldFloat(string id, string text, ref float value, int width, float step, float step_fast, string format)
        {
            IG.ImGui.Text(text); IG.ImGui.SameLine();
            IG.ImGui.SetNextItemWidth(width);
            return IG.ImGui.InputFloat("##" + id, ref value, step, step_fast, format);
        }

    }
}
