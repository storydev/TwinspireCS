﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TwinspireCS
{
    public class PackageUpdatePath
    {

        public int PackageIndex;
        public Uri? PackageUri;
        public Uri? VersionUri;
        public int MinimumVersion;
        public bool ExtractDownloadUriFromVersionFile;
        public char DownloadUriSplitChar;

        public PackageUpdatePath()
        {
            PackageIndex = -1;
            MinimumVersion = 0;
        }

    }
}
