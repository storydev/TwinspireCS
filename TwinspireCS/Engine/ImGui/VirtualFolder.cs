﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TwinspireCS.Engine.ImGui
{
    public class VirtualFolder
    {

        public string Name;
        public string FullPath;
        public bool Open;
        public List<VirtualFolder> VirtualFolders;
        public List<string> Files;

        public VirtualFolder()
        {
            Name = string.Empty;
            FullPath = string.Empty;
            VirtualFolders = new List<VirtualFolder>();
            Files = new List<string>();
        }

    }
}
