﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TwinspireCS.ImExtensions
{
    public enum ViewMapFieldType
    {
        SingleLine,
        Multiline,
        Integer,
        Float,
        Check,
        Date,
        Time,
        DateTime,

    }
}
