﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TwinspireCS.ImExtensions
{
    public class ViewMapFieldState
    {

        public ViewMapFieldType Type;
        public string Group;
        public int Max;
        public int Min;
        public int Step;

        public float MaxF;
        public float MinF;
        public float StepF;


        public string[] ComboList;
        public int ComboListIndex;
        public string FrameName;


    }
}
