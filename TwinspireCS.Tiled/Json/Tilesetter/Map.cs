﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace TwinspireCS.Tiled.Json.Tilesetter
{
    public class Map
    {

        [JsonInclude]
        public int tile_size;
        [JsonInclude]
        public int map_width;
        [JsonInclude]
        public int map_height;
        [JsonInclude]
        public IEnumerable<Layer> layers;

        public Map()
        {
            layers = new List<Layer>();
        }

    }
}
