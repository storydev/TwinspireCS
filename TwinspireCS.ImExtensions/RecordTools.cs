﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TwinspireCS.ImExtensions
{
    public class RecordTools
    {

        public SearchConfig config;
        public bool NewRecord;
        public bool EditRecord;
        public bool DeleteRecord;
        public bool CopyRecordRef;

        public RecordTools()
        {

        }

        public object CreateRecord()
        {
            var instance = Activator.CreateInstance(config.InstanceType);
            return instance;
        }



    }
}
