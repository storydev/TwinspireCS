﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace TwinspireCS.Tiled.Json.Tilesetter
{
    public class Layer
    {

        [JsonInclude]
        public string name;
        [JsonInclude]
        public IEnumerable<Position> positions;

        public Layer()
        {
            name = string.Empty;
            positions = new List<Position>();
        }

    }
}
