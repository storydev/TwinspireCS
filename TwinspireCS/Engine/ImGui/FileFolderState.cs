﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;

namespace TwinspireCS.Engine.ImGui
{
    public class FileFolderState
    {

        public string ID { get; internal set; }
        public FileFolderScope Scope { get; internal set; }
        public string SelectedPath { get; internal set; }
        public string[] SelectedPaths
        { 
            get
            {
                var splitted = SelectedPath.Split(new char[] { ';' }, StringSplitOptions.RemoveEmptyEntries);
                return splitted;
            }
        }
        public string LastCreatedPath { get; internal set; }
        public string HighlightedPath { get; internal set; }
        public Vector2 Size { get; set; }

        public string ItemPopupID { get; set; }

        // For Virtual Scope

        public List<VirtualFolder> VirtualFolders { get; internal set; }
        public List<string> VirtualFiles { get; internal set; }
        public string BrowseFolderRoute { get; internal set; }
        internal int SelectedFileIndex { get; set; }

        // For Physical Scope
        public string RootPath { get; internal set; }
        public string[][] Folders { get; internal set; }
        public string[][] Files { get; internal set; }

        public FileFolderState()
        {
            ID = string.Empty;
            VirtualFolders = new List<VirtualFolder>();
            RootPath = string.Empty;
            Folders = Array.Empty<string[]>();
            Files = Array.Empty<string[]>();
            VirtualFiles = new List<string>();
            BrowseFolderRoute = string.Empty;
            SelectedPath = string.Empty;
            LastCreatedPath = string.Empty;
            HighlightedPath = string.Empty;
            SelectedFileIndex = -1;
        }


    }
}
