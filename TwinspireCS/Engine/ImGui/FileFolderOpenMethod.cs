﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TwinspireCS.Engine.ImGui
{
    public enum FileFolderOpenMethod
    {
        /// <summary>
        /// No tools for opening or saving anything.
        /// </summary>
        None,
        /// <summary>
        /// Shows a tool to open a folder and displays only folders in the results.
        /// </summary>
        OpenFolder,
        /// <summary>
        /// Shows a tool to open (a) file(s) and displays folders and files in the results.
        /// </summary>
        OpenFile,
        /// <summary>
        /// Shows a tool to save a file and displays folders and files in the results.
        /// </summary>
        SaveFile
    }
}
