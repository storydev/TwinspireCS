﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TwinspireCS.Engine.ImGui
{
    public enum FileFolderResult
    {
        None,
        SelectedPath,
        CreatePath,
        OpenFolder,
        HighlightPath
    }
}
