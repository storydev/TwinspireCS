﻿using Raylib_cs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;
using TwinspireCS;
using TwinspireCS.Engine.Graphics;
using TwinspireCS.Tiled.Json.Tilesetter;

namespace TwinspireCS.Tiled
{
    public class Tilesetter
    {

        /// <summary>
        /// Loads a Tilesetter map from the given folder. A "txt" and "png" file of the same name as the folder must be contained
        /// in the folder for this method to work, following the conventions of Tilesetter.
        /// </summary>
        /// <param name="folder">The folder to load from.</param>
        /// <param name="mapName">The name of the map to give once loaded.</param>
        /// <param name="tilesetName">The name of the tileset image to give once loaded.</param>
        public static void LoadFromFolder(string folder, string mapName, string tilesetName)
        {
            if (!Utils.ValidateFilePath(folder, false))
                return;

            var dirName = Path.GetDirectoryName(folder);
            var jsonContent = File.ReadAllText(Path.Combine(folder, dirName + ".txt"));
            var jsonMap = JsonSerializer.Deserialize<Map>(jsonContent);

            var imageToLoad = Path.Combine(folder, dirName + ".png");
            var image = Raylib.LoadImage(imageToLoad);
            if (!Application.Instance.ResourceManager.DoesIdentifierExist(tilesetName))
            {
                Application.Instance.ResourceManager.AddResourceImage(tilesetName, image);
            }

            var tileset = new TileSet();
            tileset.Image = tilesetName;
            TileSet.AddTileSet(tileset);

            var map = new TileMap();
            map.Name = mapName;
            map.TilesX = jsonMap.map_width;
            map.TilesY = jsonMap.map_height;

            foreach (var layer in jsonMap.layers)
            {
                var convertedLayer = TileLayer.CreateLayer(map.TilesX * map.TilesY);
                foreach (var position in layer.positions)
                {
                    var tile = new Tile();
                    var index = position.y * map.TilesX + position.x;
                    tile.Index = position.id;
                    convertedLayer.Tiles[index] = tile;
                }
            }


        }

    }
}
