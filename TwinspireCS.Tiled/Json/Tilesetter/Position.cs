﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace TwinspireCS.Tiled.Json.Tilesetter
{
    public class Position
    {
        [JsonInclude]
        public int x;
        [JsonInclude]
        public int y;
        [JsonInclude]
        public int id;

        public Position()
        {

        }

    }
}
