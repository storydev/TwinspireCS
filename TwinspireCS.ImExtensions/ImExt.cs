﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;
using ImGuiNET;

namespace TwinspireCS.ImExtensions
{
    public class ImExt
    {

        static Dictionary<string, SearchConfig> searchConfigurators;
        static Dictionary<string, SearchResults> searchResults;
        static string currentSearchID;
        static Dictionary<string, int> searchResultIndices;

        /// <summary>
        /// Perform a new search using a given configuration. The config must include the fields and type of the subject
        /// data model to perform a search on. By default, no records are displayed by using this function.
        /// </summary>
        /// <remarks>
        /// To obtain search results, the returned value has a method called <c>PerformSearch</c> which can be used to specify that
        /// the results are to be searched, followed by <c>NeedsData</c> methods which requests a data source to acquire data
        /// from.
        /// </remarks>
        /// <param name="id">The unique identifier, also passed into Dear ImGui, of the search and table.</param>
        /// <param name="config">An instance of a configurator from which the data source type is retrieved.</param>
        /// <param name="tableSize">The size of the data table as rendered.</param>
        /// <returns></returns>
        public static SearchResults BeginSearch(string id, SearchConfig config, Vector2 tableSize)
        {
            searchConfigurators ??= new Dictionary<string, SearchConfig>();
            searchResults ??= new Dictionary<string, SearchResults>();
            searchResultIndices ??= new Dictionary<string, int>();

            searchConfigurators[id] = config;
            currentSearchID = id;

            var active = ImGui.BeginTable(id, config.FieldNames.Length, ImGuiTableFlags.Borders | ImGuiTableFlags.ScrollX | ImGuiTableFlags.ScrollY | ImGuiTableFlags.SizingMask | ImGuiTableFlags.Resizable, tableSize);
            if (active)
            {
                for (int i = 0; i < config.FieldNames.Length; i++)
                {
                    ImGui.TableSetupColumn(config.FieldNames[i], ImGuiTableColumnFlags.None, config.FieldWidths[i]);
                }

                ImGui.TableSetupScrollFreeze(0, 1);
                ImGui.TableHeadersRow();
            }

            SearchResults results = null;

            if (!searchResults.ContainsKey(currentSearchID))
            {
                searchResults[currentSearchID] = new SearchResults(config);
                searchResultIndices[currentSearchID] = 0;
            }

            results = searchResults[currentSearchID];
            results.Active = active;
            if (config.ReQuery)
            {
                results.ClearResults();
                results._searchQueried = false;
                config.ReQuery = false;
            }

            return results;
        }

        public static bool StartSearchResults()
        {
            if (string.IsNullOrEmpty(currentSearchID))
                return false;

            var results = searchResults[currentSearchID];
            if (results.Data == null)
                return false;

            if (results.Data.Length == 0)
                return false;

            searchResultIndices[currentSearchID] = 0;

            return true;
        }

        public static bool SearchItem(bool selected)
        {
            if (string.IsNullOrEmpty(currentSearchID))
                return false;

            var results = searchResults[currentSearchID];
            var index = searchResultIndices[currentSearchID];
            if (results.Data == null)
                return false;

            if (results.Data.Length == 0)
                return false;

            if (results._searchQueried)
                return false;

            var row = results.Data[index];
            ImGui.TableNextRow();
            for (int i = 0; i < row.Length; i++)
            {
                ImGui.TableNextColumn();
                if (i == 0)
                {
                    selected = ImGui.Selectable(row[i], selected, ImGuiSelectableFlags.SpanAllColumns);
                }
                else
                {
                    ImGui.Text(row[i]);
                }
            }

            searchResultIndices[currentSearchID] += 1;

            return selected;
        }

        public static void EndSearch()
        {
            ImGui.EndTable();
        }

        public static RecordTools? GetRecordTools(int selectedRecordIndex = -1)
        {
            if (searchConfigurators == null)
                return null;

            if (string.IsNullOrEmpty(currentSearchID))
                return null;

            if (!searchConfigurators.ContainsKey(currentSearchID))
                return null;

            ImGui.PushID(currentSearchID);

            var results = new RecordTools();
            results.config = searchConfigurators[currentSearchID];

            if (ImGui.Button("New Record"))
            {
                results.NewRecord = true;
            }

            ImGui.SameLine();

            if (selectedRecordIndex > -1)
                ImGui.BeginDisabled();

            if (ImGui.Button("Edit Record"))
            {
                results.EditRecord = true;
            }
            ImGui.SameLine();

            if (ImGui.Button("Delete Record"))
            {
                results.DeleteRecord = true;
            }
            ImGui.SameLine();

            if (ImGui.Button("Copy Ref"))
            {
                results.CopyRecordRef = true;
            }

            if (selectedRecordIndex > -1)
                ImGui.EndDisabled();

            ImGui.PopID();

            return results;
        }


    }
}
