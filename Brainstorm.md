# Brainstorm of Ideas
This is our brainstorm, describing how all rendering and simulations should be performed and in what order.

There have been multiple attempts to build a reasonable API for render states, simulating layouts and building interfaces, which would include combining elements for general drawing, textures, 3D objects and more. But, as you may have also noticed, a lot of files were deleted from this commit.

It is best to dig deep and truly understand what is the best approach for all things appropriate for modelling our API.

## The Purposes of Modelling
Working out states is not as easy as one might expect. We can't just start building an API and expect things to work, and it needs to be consistent and modelled in a way that makes sense for all purposes.

This is why I have written this document, to help not just myself, but others, to understand this model and what is involved.

## The Model Theory
Currently, there are two methods of performing state. One is retained and the other is immediate.

* Retained State - This is most often seen in object-oriented models and are the dominant form of user interface development. It involves retaining a copy of the data and only updating when things change.
* Immediate Mode - This is used in a lot of user interface APIs now, and although it is simpler for the developer using the library to build user interfaces, it is more complicated to setup and build.

Retained States are best for more complex controls, particularly when you only want to update part of a state and not re-draw an entire interface. This is especially true when dealing with things like rich text or media such as this because calculating all the positions can be a complex, difficult task to accomplish and optimise for efficiency.

However, we also don't want to be fully retained for simple controls that can be rendered each frame, and where data doesn't need to be synced constantly to ensure the data and the state are up-to-date. Immediate Mode helps us there.

So, how about the best of both worlds? But how would we achieve this?

This is probably best thought of by starting with states.

### States
Each component would have a state, and each state would be composed of different methods of update.

 * Copy Update - By setting the subject state to "copy", we would tell our state stack that we only render what is already available and do not bother updating it until we manually tell it to.
 * Immediate - This would inherit Copy, but in our API we would simply tell the state to update every frame. This way, we enforce immediate mode where it's necessary and can turn it off at will.

From here, we can determine how we construct our states and when.

In Immediate Mode APIs, layout is not pre-determined. We need to calculate where things are likely to be before we start rendering in order to perform mouse and other input events, unlike in Retained Mode UI where layout is pre-determined and events are often triggered and the action submitted to a listener. Both of these options are not options we can feasibly use in our context.

However, it is worth noting that we need to know where things are before we start rendering. Do we calculate layout like in Immediate Mode or like in Retained Mode?

Certain questions arise when working out what what should be a priority.

 1. In multi-line text formatted situations, we should calculate positions of words and lines depending on where other elements exist. We should therefore calculate all other elements first to calculate word-wrapping. Do we follow this behaviour like on the Web by default, or should all text be bound by specific dimensions?
 2. When drawing things like buttons, checkboxes, radios, drop-downs, etc., do we set pre-determined positions or do we calculate based on text position like on the Web? Do we do both?
	1. If we go ahead and use Web-like layouting for multi-line text situations, then we need to know the positions and sizes of elements that follow this same behaviour in advance before rendering. Although this would be typical of a retained mode interface, it does offer the simpler solution. The issue here is we are telling the state of all following this layout logic that they should stay in position and at their fixed sizes at all time. Then the question becomes, can we tell these elements to be "Immediate" for data sync purposes? We should be able to use this model, but it may complicate things.
	2. If we instead separate rich text media from interactive elements, we can make things a lot simpler for ourselves. The issue is that there is less freedom for the developer.
 3. Finally, when we determine the containers that would ultimately calculate their layouts independently (assuming that's the best option), we should decide whether these containers are fixed, flow within other containers, or a mixture of both.
	1. By only allowing fixed containers, we can calculate input simulations only on containers that change (positions and sizes).
	2. By only allowing flow containers, we are effectively enforcing the word-wrapping rule and everything is calculated based on text content.
	3. We could have both. Fixed containers have their content laid out once and only when the container changes does the content get updated (besides data copies). Flow containers layout content by following the multiline text rules, so positions and sizes are determined automatically rather than supplied by the user in the case of a fixed container. This would make the codebase potentially inconsistent, but would allow for greater flexibility. In this case, we wouldn't allow containers to be mixed. They would have to be separate and their layout logic defined by the user. This separation would make it clear and identifiable where elements are drawn and where.

Following these principles, we could build a user interface API that follows immediate-mode concepts, such as returning basic values for detecting a change in state, while at the same time following traditional retained-mode concepts such as building complex multi-formatted text as an example which is not feasible or possible in the immediate-mode model. This way, we don't sacrifice programming productivity for complex user interfaces.

So, how do we do this? Let's discuss our answers.

### Answers

 1. In multi-line text formatted situations, we should calculate positions of words and lines depending on where other elements exist. We should therefore calculate all other elements first to calculate word-wrapping. Do we follow this behaviour like on the Web by default, or should all text be bound by specific dimensions?

_In desktop or game applications, user interfaces tend to be built using bounding dimensions for everything. This includes text. On the Web, this is completely different. Web interfaces layout things by automatically flowing right, then downwards, depending on the CSS `position` and `display` rules._

_While both have merits, neither are truly workable solutions to the problem of using one interface library over another. It is without a doubt that when we wish to work on both the Web and the Desktop, or even mobile for that matter, that all UI libraries built around these platforms are designed completely differently, independent from each other and following different concepts._

_For me, I would prefer to stick to one principle. This principle should apply to all platforms, not just the Web or desktop applications._

_There are reasons why the Web works the way it does. In many respects, HTML is not necessarily the culprit for the the Web's pitfalls and problems. In fact, most of it is down to JavaScript being a terrible language, and we are stuck with the consequences. There are reasons why developers over the years have built tools around CSS and JavaScript but barely touched HTML. I suppose then that means there's almost nothing wrong with HTML, and all the problems are in CSS/JavaScript. But that's besides the point of this answer and an unnecessary rant on my part._

_However, the context I feel is important. Because although the Web is poorly designed, it does get one thing right -- layouts._

_Using text bound by specific dimensions can be useful, but this is actually the biggest pitfall of video game and desktop design. And although the interfaces may look pretty, and in some cases, better than the Web, using fixed bounding dimensions for text does force the programmer to specify the positions and sizes of everything as most user interface engines for games and desktop applications don't generally do this work by itself. ImGui paradigms do automatic layout control for a reason. Yet, the "can be useful" is still something to consider._

_Where is text bound by specific dimensions useful?_

_In almost every component for one thing, but what about text on its own affecting everything around it?_

_Where you don't want text to impact layout, like in tables for example. Even in the context of video games, it can add some sense of consistency to layouts which is important for the user experience. However, in video games text is often rendered as bitmap images rather than natively, so the benefits of automatic layouts is almost redundant and don't make sense, particularly when most games are rendered from a background buffer pre-drawn before the current frame._

_And so, the answer is not very simple. Do we go for automatic layouts in this case since it offers the most benefit on most platforms, or use bounding boxes as standard?_

_Going to the third question temporarily, if we choose to build two separate layout engines independent from each other, which will likely become the case, the default for each will be dependent on the container type. Fixed containers for desktop applications and video games, and flow containers for the Web. This makes the most sense, but it adds a lot more work._

 2. When drawing things like buttons, checkboxes, radios, drop-downs, etc., do we set pre-determined positions or do we calculate based on text position like on the Web? Do we do both?

_Being able to set the positions and sizes of interactive elements is so much easier than trying to work with automatic layouts and their problems relating to this. It all boils down to how much control you want over your interactive elements and how they are rendered._

_On the Web, a problem occurs when you are creating interactive elements in line with text. This is why they are often separated and sit in their own "containers", if you like. This is where fixed containers effectively become useful, and the multi-line multi-formatted nature of text slaps you in the face._

_What if by calling a function like `richText` opens a flow container automatically, and calling a function like `button` subsequently after closes the previous flow container and opens a new, fixed container? Woops. Did I just share trade secrets, or what? Well, okay, it's not that big a deal, but it's something that's likely better than most layout engines work._

_We could alternatively leave the different container types open to user intervention, but this would mean that `richText` calls in fixed containers won't do what they're supposed to, and `button` calls in flow containers -- well, they will work, and... yes, they will work._

_Okay, maybe we give the option to choose between fixed and flow container types, but setting pre-determined positions and sizes of interactive elements will mean that buttons and the likes are rendered quicker than their rich text counterparts. That probably doesn't matter in the grand scheme of things, since we should always have a back buffer regardless of the platform or context, we just won't stretch it where it doesn't need to._

 3. Finally, when we determine the containers that would ultimately calculate their layouts independently (assuming that's the best option), we should decide whether these containers are fixed, flow within other containers, or a mixture of both.

_Based on everything we just said above, you probably already know the answer. We're having both, but no overlaps. It doesn't make sense to us to have a flow type container inside a fixed type container, and vice versa. This complication is not necessary, not needed, and just creates hierarchy where it's not needed. Done are the days of object hierarchies. I'd prefer to move away from the DOM as well._

## Graphing the Model
And so, this leaves us at the point where we can start working out our logic.

```cs
class State {
	/*
	* This represents our state for each component drawn.
	* This would contain basic information, like position and size.
	* It might be automatically calculated depending on the type of container its' in.
	*/
}

class Container {
	/*
	* This won't be containing any states.
	* But what it does do is determine where this container is and its size.
	* What type of container depends on its layout logic.
	* It will tell us which state is rendered first within this container and which is the last.
	* It will not do clipping or wrapping, that's the state engine's job.
	*/
}

class StateEngine {
	/*
	* Contains all our states in one place in a linear array.
	* It will give us the completed and simulated state of the WHOLE canvas BEFORE drawing, with 
	* some exceptions.
	*/
}

class StateTemplate {
	/*
	* We need a way to determine how state's are drawn.
	* It's all good giving dimensions and performing layout logic, but it's useless without anything
	* telling us WHAT to render.
	* This will tell us exactly what type of thing to draw:
	*   Rectangles, Triangles, Circles, Text, Images, you name it.
	* 
	* These are not assigned to states. It's up to the state engine to tell us what to draw once
	* it gives us dimensions and the simulated state.
	*/
}

class StateRenderer {
	/*
	* We get given dimensions, states and templates, now it's time to render.
	* But where do we render to? A back buffer? Straight to the drawing routine?
	* Perhaps we give the state engine the context, and the context tells us
	* where to draw to.
	*/
}

class StateContext {
	/*
	* To the primary renderer or a back buffer? Your choice.
	*/
}
```

How simple is that, then? Well, it might be more complicated than that, but this is just the starting point.