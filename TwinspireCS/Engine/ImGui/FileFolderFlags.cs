﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TwinspireCS.Engine.ImGui
{
    [Flags]
    public enum FileFolderFlags
    {
        /// <summary>
        /// No flags set.
        /// </summary>
        None,
        /// <summary>
        /// Enables the ability to create new folders.
        /// </summary>
        AllowNewFolders        = 0x01,
        /// <summary>
        /// Allows for multi-selection.
        /// </summary>
        MultiSelect            = 0x02,
        /// <summary>
        /// Display common folder locations. Applicable to Physical Location state storage only.
        /// </summary>
        CommonFolderLocations  = 0x04,
        /// <summary>
        /// Completely disable open/save tool options.
        /// </summary>
        NoTools                = 0x08,
    }
}
