﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Reflection;
using IG = ImGuiNET;
using ImGuiNET;
using System.Numerics;

namespace TwinspireCS.Engine.ImGui
{
    public class Helpers
    {

        private static Dictionary<string, FileFolderState> fileFolderStates;

        private static Dictionary<string, SearchData> searchData;
        private static string currentlyActiveId;

        /// <summary>
        /// Begins a table with filters and the ability to search through the data.
        /// Uses the ImGui Table API.
        /// </summary>
        /// <param name="id">The ID for the table.</param>
        /// <param name="type">The type of the class from which the data should be extracted.</param>
        /// <param name="options">Use a preset form of options to determine behaviour and appearance of the table.</param>
        public static bool BeginSearchTable(string id, Type type, SearchOptions? options = null)
        {
            options ??= new SearchOptions();
            searchData ??= new Dictionary<string, SearchData>();
            if (!searchData.ContainsKey(id))
            {
                var search = new SearchData();
                var fields = type.GetFields(BindingFlags.Public | BindingFlags.Instance);
                search.fieldNames = new string[fields.Length];
                search.fieldTypes = new Type[fields.Length];
                search.fieldWidths = new float[fields.Length];
                search.fieldRelationship = new Type[fields.Length];
                search.fieldRelationshipFieldName = new string[fields.Length];
                search.fieldFilters = new object[fields.Length];
                for (int i = 0; i < fields.Length; i++)
                {
                    search.fieldNames[i] = fields[i].Name;
                    search.fieldTypes[i] = fields[i].FieldType;
                    if (options.Options.ContainsKey(search.fieldNames[i]))
                    {
                        var fieldOptions = options.Options[search.fieldNames[i]];
                        search.fieldWidths[i] = fieldOptions.Width;
                        if (fieldOptions.RelationshipType != null)
                        {
                            search.fieldRelationship[i] = fieldOptions.RelationshipType;
                            search.fieldRelationshipFieldName[i] = fieldOptions.RelationshipFieldName;
                        }
                    }
                    else
                    {
                        search.fieldWidths[i] = 150f;
                    }
                    
                    
                }
                searchData.Add(id, search);
            }

            var data = searchData[id];
            for (int i = 0; i < data.fieldNames.Length; i++)
            {
                IG.ImGui.TableSetupColumn(data.fieldNames[i], IG.ImGuiTableColumnFlags.None, data.fieldWidths[i]);
            }

            IG.ImGuiTableFlags flags = IG.ImGuiTableFlags.None;
            if (options.CanOrderColumns)
                flags |= IG.ImGuiTableFlags.Reorderable;

            flags |= IG.ImGuiTableFlags.Resizable;
            flags |= IG.ImGuiTableFlags.Sortable;
            flags |= IG.ImGuiTableFlags.ContextMenuInBody;

            var isOpen = IG.ImGui.BeginTable(id, data.fieldNames.Length, flags);
            if (isOpen)
            {
                IG.ImGui.TableNextRow(IG.ImGuiTableRowFlags.Headers);
                IG.ImGui.TableNextRow();

                for (int i = 0; i < data.fieldFilters.Length; i++)
                {
                    var filterType = data.fieldTypes[i];
                    if (filterType == typeof(string))
                    {
                        string value = string.Empty;
                        if (data.fieldFilters[i] != null)
                        {
                            value = data.fieldFilters[i].ToString();
                        }
                        IG.ImGui.InputText("##Filter" + i, ref value, 256);
                        data.fieldFilters[i] = value;
                    }
                    else if (filterType == typeof(int))
                    {
                        int value = 0;
                        if (data.fieldFilters[i] != null)
                        {
                            value = (int)data.fieldFilters[i];
                        }
                        IG.ImGui.InputInt("##Filter" + i, ref value);
                        data.fieldFilters[i] = value;
                    }
                    else if (filterType == typeof(float))
                    {
                        float value = 0;
                        if (data.fieldFilters[i] != null)
                        {
                            value = (float)data.fieldFilters[i];
                        }
                        IG.ImGui.InputFloat("##Filter" + i, ref value);
                        data.fieldFilters[i] = value;
                    }
                    else if (filterType == typeof(double))
                    {
                        double value = 0;
                        if (data.fieldFilters[i] != null)
                        {
                            value = (double)data.fieldFilters[i];
                        }
                        IG.ImGui.InputDouble("##Filter" + i, ref value);
                        data.fieldFilters[i] = value;
                    }
                    else if (filterType == typeof(bool))
                    {
                        bool value = false;
                        if (data.fieldFilters[i] != null)
                        {
                            value = (bool)data.fieldFilters[i];
                        }
                        IG.ImGui.Checkbox("##Filter" + i, ref value);
                        data.fieldFilters[i] = value;
                    }


                    IG.ImGui.TableNextColumn();
                }

                currentlyActiveId = id;
            }

            return isOpen;
        }

        /// <summary>
        /// The data to use for populating this table. It is best to pre-load data before rendering.
        /// Specify the range of the data to display. If using thousands of records, it is best to
        /// specify start and end for efficiency.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="data">The data to render into the table.</param>
        /// <param name="start">The beginning of the range to acquire from data.</param>
        /// <param name="end">The end of the range to acquire from data.</param>
        public static void SearchTableNeedsData<T>(IEnumerable<T> data, int start, int end)
        {
            for (int i = start; i < end; i++)
            {
                var item = data.ElementAt(i);
                IG.ImGui.TableNextRow();


            }
        }

        /// <summary>
        /// Create a file dialog state with the given ID. Use Begin*, End* calls for rendering the state as a popup modal. Otherwise, just
        /// call <c>RenderState</c>.
        /// </summary>
        /// <param name="id">Specifies the ID, thus a popup modal, if using a popup modal.</param>
        /// <param name="scope">The scope determines if we are using the native file system or our own custom virtual storage.</param>
        /// <param name="size">The size of the modal or content.</param>
        /// <returns></returns>
        public static FileFolderState CreateFileDialog(string id, FileFolderScope scope, Vector2 size)
        {
            fileFolderStates ??= new Dictionary<string, FileFolderState>();
            if (!fileFolderStates.ContainsKey(id))
            {
                fileFolderStates[id] = new FileFolderState();
                fileFolderStates[id].Scope = scope;
                fileFolderStates[id].Size = size;
                fileFolderStates[id].ID = id;
            }

            return fileFolderStates[id];
        }

        public static void SetFileRootPath(ref FileFolderState state, string path)
        {
            if (state == null) return;

            if (state.Scope != FileFolderScope.PhysicalLocation) return;

            if (Directory.Exists(path))
            {
                state.Folders = new string[1][];
                state.Folders[0] = Directory.GetDirectories(path);
                state.Files = new string[state.Folders.Length][];
                for (int i = 0; i < state.Folders.Length; i++)
                {
                    state.Files[i] = Directory.GetFiles(state.Folders[0][i]);
                }
            }
        }

        public static void SetVirtualFolders(ref FileFolderState state, List<VirtualFolder> virtualFolders)
        {
            state.VirtualFolders = virtualFolders;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="state"></param>
        /// <param name="display"></param>
        /// <param name="openMethod"></param>
        /// <param name="allowNewFolders"></param>
        /// <returns></returns>
        public static FileFolderResult BeginFileDialog(ref FileFolderState state, FileFolderDisplay display, FileFolderOpenMethod openMethod, FileFolderFlags flags = FileFolderFlags.AllowNewFolders)
        {
            var open = IG.ImGui.BeginPopupModal(state.ID);

            if (!open)
                return FileFolderResult.None;

            return RenderState(ref state, display, openMethod, flags);
        }

        public static FileFolderResult RenderState(ref FileFolderState state, FileFolderDisplay display, FileFolderOpenMethod openMethod, FileFolderFlags flags = FileFolderFlags.AllowNewFolders)
        {
            if (display == FileFolderDisplay.Flat && openMethod == FileFolderOpenMethod.OpenFolder)
            {
                return RenderBrowseFlat(ref state, flags);
            }
            else if (display == FileFolderDisplay.Split)
            {
                return RenderBrowseSplit(ref state, openMethod, flags);
            }

            return FileFolderResult.None;
        }

        public static void EndFileDialog()
        {
            IG.ImGui.EndPopup();
        }

        static string newPath = string.Empty;

        static void CheckVirtualFolderPaths(VirtualFolder folder, string currentPath)
        {
            foreach (var vf in folder.VirtualFolders)
            {
                if (string.IsNullOrEmpty(vf.FullPath))
                {
                    vf.FullPath = currentPath + "/" + vf.Name;
                    CheckVirtualFolderPaths(vf, vf.FullPath);
                }
            }
        }

        static FileFolderResult RenderBrowseSplit(ref FileFolderState state, FileFolderOpenMethod openMethod, FileFolderFlags fileFolderFlags = FileFolderFlags.AllowNewFolders)
        {
            FileFolderResult result = FileFolderResult.None;
            if (fileFolderFlags.HasFlag(FileFolderFlags.MultiSelect) && openMethod == FileFolderOpenMethod.SaveFile)
            {
                throw new Exception("You cannot use the MultiSelect flag with the SaveFile open method.");
            }

            if (fileFolderFlags.HasFlag(FileFolderFlags.AllowNewFolders))
            {
                if (IG.ImGui.Button("New Folder##NewVirtualFolder"))
                {
                    IG.ImGui.OpenPopup("CreateNewVirtualFolder");
                }
            }

            IG.ImGui.SameLine();

            if (string.IsNullOrEmpty(state.BrowseFolderRoute))
                IG.ImGui.BeginDisabled();

            var selectText = "Select Folder(s)";
            if (openMethod == FileFolderOpenMethod.OpenFile)
            {
                selectText = "Open File(s)";
            }
            else if (openMethod == FileFolderOpenMethod.SaveFile)
            {
                selectText = "Save File";
            }
            
            if (!fileFolderFlags.HasFlag(FileFolderFlags.NoTools))
            {
                if (IG.ImGui.Button(selectText))
                {
                    if (fileFolderFlags.HasFlag(FileFolderFlags.MultiSelect) && openMethod != FileFolderOpenMethod.SaveFile)
                    {
                        if (openMethod == FileFolderOpenMethod.OpenFolder)
                        {

                        }

                        result = FileFolderResult.SelectedPath;
                        if (IG.ImGui.IsPopupOpen(state.ID))
                            IG.ImGui.CloseCurrentPopup();
                    }
                    else if (!fileFolderFlags.HasFlag(FileFolderFlags.MultiSelect))
                    {
                        if (openMethod == FileFolderOpenMethod.OpenFolder)
                            state.SelectedPath = state.BrowseFolderRoute;

                        result = FileFolderResult.SelectedPath;
                        if (IG.ImGui.IsPopupOpen(state.ID))
                            IG.ImGui.CloseCurrentPopup();
                    }
                }
            }

            if (string.IsNullOrEmpty(state.BrowseFolderRoute))
                IG.ImGui.EndDisabled();

            if (IG.ImGui.BeginTable("SplitFolderList", 2, ImGuiTableFlags.BordersInner | ImGuiTableFlags.PreciseWidths, state.Size))
            {
                var half = state.Size.X / 2;
                IG.ImGui.TableSetupColumn("Folders", ImGuiTableColumnFlags.NoHeaderLabel, half);
                IG.ImGui.TableSetupColumn("Files", ImGuiTableColumnFlags.NoHeaderLabel, half);
                IG.ImGui.TableNextRow();

                IG.ImGui.TableSetColumnIndex(0);
                if (state.Scope == FileFolderScope.VirtualStorage)
                {
                    RenderRootTreeFolderList(ref state, (int)half, out FileFolderResult treeResult);
                    if (result == FileFolderResult.None && treeResult != FileFolderResult.None)
                    {
                        result = treeResult;
                        state.SelectedFileIndex = -1;
                    }

                }

                IG.ImGui.TableSetColumnIndex(1);

                if (state.Scope == FileFolderScope.VirtualStorage)
                {
                    var folder = GetFolderFromPath(state, state.BrowseFolderRoute);
                    List<string> files = null;
                    if (folder == null) // assume we're at root (/)
                    {
                        files = state.VirtualFiles;
                    }
                    else
                    {
                        files = folder.Files;
                    }

                    IG.ImGui.BeginChild("FileList", new Vector2(half, state.Size.Y));

                    for (int i = 0; i < files.Count; i++)
                    {
                        IG.ImGui.PushID(i);
                        if (IG.ImGui.Selectable(files[i], state.SelectedFileIndex == i))
                        {
                            state.SelectedFileIndex = i;
                            state.SelectedPath = state.BrowseFolderRoute + "/" + files[i];
                            if (result == FileFolderResult.None)
                                result = FileFolderResult.SelectedPath;
                        }

                        if (IG.ImGui.IsItemHovered())
                        {
                            state.HighlightedPath = state.BrowseFolderRoute + "/" + files[i];
                            if (result == FileFolderResult.None)
                                result = FileFolderResult.HighlightPath;

                            if (!string.IsNullOrEmpty(state.ItemPopupID))
                            {
                                
                            }
                        }

                        IG.ImGui.PopID();
                    }
                    IG.ImGui.EndChild();
                }

                IG.ImGui.EndTable();
            }

            return result;
        }

        static FileFolderResult RenderBrowseFlat(ref FileFolderState state, FileFolderFlags fileFolderFlags = FileFolderFlags.AllowNewFolders)
        {
            FileFolderResult result = FileFolderResult.None;
            if (fileFolderFlags.HasFlag(FileFolderFlags.AllowNewFolders))
            {
                if (IG.ImGui.Button("New Folder##NewVirtualFolder"))
                {
                    IG.ImGui.OpenPopup("CreateNewVirtualFolder");
                }
            }

            IG.ImGui.SameLine();

            if (string.IsNullOrEmpty(state.BrowseFolderRoute))
                IG.ImGui.BeginDisabled();

            if (IG.ImGui.Button("Select Folder##SelectVirtualFolder"))
            {
                state.SelectedPath = state.BrowseFolderRoute;
                result = FileFolderResult.SelectedPath;
                IG.ImGui.CloseCurrentPopup();
            }

            if (string.IsNullOrEmpty(state.BrowseFolderRoute))
                IG.ImGui.EndDisabled();

            if (IG.ImGui.BeginPopup("CreateNewVirtualFolder"))
            {
                IG.ImGui.Text("Create in: " + (string.IsNullOrEmpty(state.BrowseFolderRoute) ? "/" : state.BrowseFolderRoute));
                IG.ImGui.Text("Name:");
                IG.ImGui.SameLine();
                IG.ImGui.SetNextItemWidth(125f);
                IG.ImGui.InputText("##CreateNewVirtualFolderPath", ref newPath, 64);
                if (IG.ImGui.Button("Create##ConfirmCreateNewVirtualFolder"))
                {
                    if (!string.IsNullOrEmpty(state.BrowseFolderRoute))
                    {
                        var virtualFolder = GetFolderFromPath(state, state.BrowseFolderRoute);
                        virtualFolder?.VirtualFolders.Add(new VirtualFolder()
                        {
                            Name = newPath,
                            FullPath = state.BrowseFolderRoute + "/" + newPath,
                        });

                        IG.ImGui.CloseCurrentPopup();
                    }
                    else
                    {
                        state.VirtualFolders.Add(new VirtualFolder()
                        {
                            Name = newPath,
                            FullPath = newPath
                        });
                        IG.ImGui.CloseCurrentPopup();
                    }

                    newPath = string.Empty;
                    result = FileFolderResult.CreatePath;
                }
                IG.ImGui.SameLine();

                if (IG.ImGui.Button("Cancel##CancelCreateNewVirtualFolder"))
                {
                    IG.ImGui.CloseCurrentPopup();
                }

                IG.ImGui.EndPopup();
            }

            IG.ImGui.Separator();

            RenderRootTreeFolderList(ref state, 300, out FileFolderResult treeResult);

            if (result == FileFolderResult.None && treeResult != FileFolderResult.None)
                result = treeResult;

            IG.ImGui.Text("Selected Path: " + state.BrowseFolderRoute);

            return result;
        }

        static bool IsAnyChildFolderSelected(VirtualFolder folder)
        {
            var result = false;
            foreach (var vf in folder.VirtualFolders)
            {
                if (vf.Open)
                {
                    result = true;
                    break;
                }
            }
            return result;
        }

        static void RenderRootTreeFolderList(ref FileFolderState state, int width, out FileFolderResult result)
        {
            IG.ImGui.BeginChild("VirtualFolderTreeList", new Vector2(width, 300));

            int openThisFolder = -1;
            for (int i = 0; i < state.VirtualFolders.Count; i++)
            {
                var folder = state.VirtualFolders[i];
                var flags = ImGuiTreeNodeFlags.OpenOnArrow | ImGuiTreeNodeFlags.SpanAvailWidth | ImGuiTreeNodeFlags.OpenOnDoubleClick;
                if (folder.Open)
                    flags |= ImGuiTreeNodeFlags.Selected;

                if (folder.VirtualFolders.Count == 0)
                    flags |= ImGuiTreeNodeFlags.Leaf | ImGuiTreeNodeFlags.NoTreePushOnOpen;
                
                if (state.VirtualFiles.Count > 0 && i == 0)
                {
                    var noFolderOpen = !IsAnyFolderOpen(state);
                    var rootFlags = ImGuiTreeNodeFlags.SpanAvailWidth | ImGuiTreeNodeFlags.NoTreePushOnOpen | ImGuiTreeNodeFlags.NoAutoOpenOnLog;
                    if (noFolderOpen)
                        rootFlags |= ImGuiTreeNodeFlags.Selected;

                    IG.ImGui.TreeNodeEx("/##RootPath", rootFlags);
                    if (IG.ImGui.IsItemClicked() && !IG.ImGui.IsItemToggledOpen())
                    {
                        foreach (var vf in state.VirtualFolders)
                        {
                            vf.Open = false;
                            FalsifyFromFolder(vf);
                        }

                        state.SelectedPath = "/";
                        state.BrowseFolderRoute = "/";
                        result = FileFolderResult.SelectedPath;
                    }
                }

                bool nodeOpen = IG.ImGui.TreeNodeEx(folder.Name, flags);
                if (IG.ImGui.IsItemClicked() && !IG.ImGui.IsItemToggledOpen())
                {
                    openThisFolder = i;
                    state.SelectedFileIndex = -1;
                }

                if (IG.ImGui.IsItemHovered())
                {
                    state.HighlightedPath = folder.FullPath;
                }

                if (nodeOpen)
                {
                    RenderTreeFolderList(ref state, folder, out result);

                    if (folder.VirtualFolders.Count > 0)
                        IG.ImGui.TreePop();
                }
            }

            if (openThisFolder > -1)
            {
                SetSelectedPath(ref state, state.VirtualFolders[openThisFolder].FullPath);
                result = FileFolderResult.OpenFolder;
            }
            else
            {
                result = FileFolderResult.None;
            }
            
            IG.ImGui.EndChild();
        }

        static void FalsifyFromFolder(VirtualFolder folder)
        {
            foreach (var vf in folder.VirtualFolders)
            {
                vf.Open = false;
                FalsifyFromFolder(vf);
            }
        }

        static void RenderTreeFolderList(ref FileFolderState state, VirtualFolder vFolder, out FileFolderResult result)
        {
            if (vFolder.VirtualFolders.Count == 0)
            {
                result = FileFolderResult.None;
                return;
            }

            int openThisFolder = -1;
            for (int i = 0; i < vFolder.VirtualFolders.Count; i++)
            {
                var folder = vFolder.VirtualFolders[i];
                var flags = ImGuiTreeNodeFlags.OpenOnArrow | ImGuiTreeNodeFlags.SpanAvailWidth | ImGuiTreeNodeFlags.OpenOnDoubleClick;
                if (folder.Open)
                    flags |= ImGuiTreeNodeFlags.Selected;

                if (folder.VirtualFolders.Count == 0)
                    flags |= ImGuiTreeNodeFlags.Leaf | ImGuiTreeNodeFlags.NoTreePushOnOpen;

                bool nodeOpen = IG.ImGui.TreeNodeEx(folder.Name, flags);
                if (IG.ImGui.IsItemClicked() && !IG.ImGui.IsItemToggledOpen())
                {
                    openThisFolder = i;
                }

                if (IG.ImGui.IsItemHovered())
                {
                    state.HighlightedPath = folder.FullPath;
                }

                if (nodeOpen)
                {
                    RenderTreeFolderList(ref state, folder, out result);

                    if (folder.VirtualFolders.Count > 0)
                        IG.ImGui.TreePop();
                }
            }

            if (openThisFolder > -1)
            {
                SetSelectedPath(ref state, vFolder.VirtualFolders[openThisFolder].FullPath);
                result = FileFolderResult.OpenFolder;
                return;
            }

            result = FileFolderResult.None;
        }

        static void SetSelectedPath(ref FileFolderState state, string path)
        {
            for (int k = 0; k < state.VirtualFolders.Count; k++)
            {
                state.VirtualFolders[k].Open = false;
                FalsifyFromFolder(state.VirtualFolders[k]);
            }

            var splitted = path.Split(new char[] { '\\', '/' }, StringSplitOptions.RemoveEmptyEntries);
            VirtualFolder currentFolder = null;
            for (int i = 0; i < splitted.Length; i++)
            {
                if (string.IsNullOrEmpty(splitted[i]))
                    continue;

                if (i == 0)
                {
                    foreach (var folder in state.VirtualFolders)
                    {
                        if (folder.Name == splitted[i])
                        {
                            folder.Open = true;
                            currentFolder = folder;
                            break;
                        }
                    }
                }
                else
                {
                    VirtualFolder temp = null;
                    foreach (var folder in currentFolder?.VirtualFolders)
                    {
                        if (folder.Name == splitted[i])
                        {
                            folder.Open = true;
                            temp = folder;
                            break;
                        }
                    }

                    currentFolder = temp;
                }
            }

            state.BrowseFolderRoute = path;
        }

        static bool IsAnyFolderOpen(FileFolderState state, VirtualFolder? folder = null)
        {
            var folders = folder?.VirtualFolders ?? state.VirtualFolders;
            var result = false;
            foreach (var vf in folders)
            {
                if (vf.Open)
                {
                    result = true;
                    break;
                }
                result = IsAnyFolderOpen(state, vf);
                if (result)
                    break;
            }
            return result;
        }

        static string DetermineSelectedFolder(FileFolderState state, VirtualFolder? folder = null)
        {
            var finalResult = string.Empty;
            var root = folder?.VirtualFolders ?? state.VirtualFolders;
            foreach (var f in root)
            {
                if (f.Open)
                {
                    finalResult += f.Name + "/" + DetermineSelectedFolder(state, f);
                    break;
                }
            }
            return finalResult;
        }

        public static VirtualFolder? GetFolderFromPath(FileFolderState state, string path, VirtualFolder? current = null)
        {
            var splitted = path.Split(new char[] { '\\', '/' }, StringSplitOptions.RemoveEmptyEntries);
            if (splitted.Length == 0)
                return null;

            var folder = splitted[0];
            var virtualFolder = current?.VirtualFolders ?? state.VirtualFolders;

            foreach (var v in virtualFolder)
            {
                if (v.Name == folder)
                {
                    var nextSlashIndex = path.IndexOfAny(new char[] { '\\', '/' });
                    if (nextSlashIndex == -1 || nextSlashIndex == path.Length - 1)
                        return v;

                    var nextPath = path[(nextSlashIndex + 1)..];
                    return GetFolderFromPath(state, nextPath, v);
                }
            }

            return null;
        }

    }
}
